add_namespace = unclaimed

country_event = {
	id = unclaimed.1

	hidden = yes # Optional : if set to "yes" no one will see the event

	fire_only_once = yes # Optional : if set to "yes" the event will never occur more than once

	immediate = { # Optional : all the following will occur once the event occurs
		if = {
			limit = {
				original_tag = UNC
			}
			set_rule = { 
			can_not_declare_war = yes 
			can_join_factions = no
			can_create_factions = no
			can_boost_other_ideologies = no
			can_be_called_to_war = no
			} 
		}
	}
}